import math

class Vertex():
    def __init__(self, coordenates, name, vertices):
        self.x = coordenates[0]
        self.y = coordenates[1]
        self.name = name
        self.vertices = vertices

    def __str__(self):
        return ("Name: " + self.name + " | " + "posX: " + str(self.x) +
                " | posY: " + str(self.y) + " | Vertex : " + str(self.vertices))

    @staticmethod
    def get_by_name(name, vertices):
        for vertex in vertices:
            if vertex.name == name:
                return vertex


class Edge():
    def __init__(self, start_vertex, end_vertex):
        self.start_vertex = start_vertex
        self.end_vertex = end_vertex
        self.name = start_vertex.name + end_vertex.name
        if end_vertex != None:
            self.distance = (math.sqrt(math.pow(
                end_vertex.x - (start_vertex.x), 2) + math.pow(end_vertex.y - (start_vertex.y), 2)))
        else:
            self.distance = 0
    
    def __str__(self):
        end_vertex_name = ""
        if  self.end_vertex != None:
            end_vertex_name = self.end_vertex.name
        return ("Aresta: " + self.start_vertex.name + " -> " + end_vertex_name + " | " + "Custo: " + str(round(self.distance,2)))

    @staticmethod
    def get_midpoint(x1, x2, y1, y2):
        x = (x1 + x2) / 2
        y = (y1 + y2) / 2
        return (x, y)
