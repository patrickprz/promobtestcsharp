from flask import Flask, render_template
import json
from flask import Response, jsonify, request, send_file, make_response, jsonify
from util import Util
from view import plot_graph
app = Flask(__name__)
from flask.ext.api import status

@app.route('/draw/', methods=['GET', 'POST'])
def draw():
    data = request.json
    vertices = Util.load_data(data)
    edges = Util.create_edges(vertices)

    plot_graph(vertices, edges, "Grafo")
    #filename = 'graph.png'
    #return send_file(filename, mimetype='image/png')
    return HTTP

@app.route('/image/', methods=['GET'])
def image():
    return send_file('graph.png', mimetype='image/png')

if __name__ == '__main__':
    app.run(host='104.131.25.255', port=5000)
    #app.run()