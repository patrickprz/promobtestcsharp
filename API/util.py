import json
from models import Vertex, Edge

class Util():
    @staticmethod
    def load_data(data):
        vertices = [] 

        for vertex in data:
            coord = list((map(float, vertex["Coordenates"].split(","))))
            name = (vertex["Name"])
            vert = (vertex["Vertices"])
            vertex = Vertex(coord, name, vert)
            vertices.append(vertex)

        return vertices

    @staticmethod
    def create_edges(vertices):
        edges = []

        for start_vertex in vertices:
            for end_vertex_name in start_vertex.vertices:
                end_vertex = Vertex.get_by_name(end_vertex_name, vertices)
                edge = Edge(start_vertex, end_vertex)
                edges.append(edge)

        return edges
