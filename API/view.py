import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from models import Edge

def plot_graph(vertices, edges, title):
    fig, ax = plt.subplots()

    for vertex in vertices:
        ax.add_artist(plt.Circle((vertex.x, vertex.y), radius=1))
        
        ax.text(
            vertex.x,
            vertex.y,
            vertex.name,
            fontsize=12,
            color='white',
            verticalalignment='center',
            horizontalalignment='center',
            fontweight='bold')

    for edge in edges:
        if edge.end_vertex != None: 
            plt.plot([edge.start_vertex.x, edge.end_vertex.x], 
                    [edge.start_vertex.y, edge.end_vertex.y])

            midpoint_x, midpoint_y = Edge.get_midpoint(edge.start_vertex.x, edge.end_vertex.x, edge.start_vertex.y, edge.end_vertex.y)

            distance = edge.distance
        
            ax.text(
                midpoint_x,
                midpoint_y,
                str(round(distance, 2)),
                fontsize=11,
                color='black',
                fontweight='bold',
                verticalalignment='center',
                horizontalalignment='center')

    DPI = fig.get_dpi()
    fig.set_size_inches(900.0/float(DPI),460.0/float(DPI))
    ax.axis('equal')
    ax.set_title(title, fontsize=10)
    fig.tight_layout()
    fig.savefig('graph.png')
    plt.close(fig)
    #plt.show()