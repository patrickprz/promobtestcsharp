﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TesteGrafosPatrick.Models;

namespace TesteGrafosPatrick.Applications
{
    class Eulerian
    {
        private List<Vertex> verifyEulerianPath(List<Vertex> vertices, List<Edge> edges)
        {
            int degreeCounter = 0;
            List<Vertex> startVertices = new List<Vertex>();
            Dictionary<Vertex, int> degrees = new Dictionary<Vertex, int>();
            degrees = Vertex.GetDegree(vertices, edges);

            foreach (var degree in degrees)
            {
                if (degree.Value == 0)
                {
                    return null;
                }
                else if (degree.Value%2 != 0)
                {
                    startVertices.Add(degree.Key);
                    degreeCounter++;
                }
            }
            if (degreeCounter > 2)
            {
                return null;
            }
            return startVertices;
        }

        private List<Edge> travel (Edge startEdge, List<Edge> edges)
        {
            List<Edge> traveledEdges = new List<Edge>();
            traveledEdges.Add(startEdge);

            for (int i = 0; i < edges.Count(); i++)
            {
                if (startEdge != null)
                {
                    startEdge = Edge.GetByVertex(startEdge.EndVertex, edges, traveledEdges);
                    traveledEdges.Add(startEdge);
                }
            }
            return traveledEdges;
        }

        public List<Edge> EulerianPath(List<Vertex> vertices, List<Edge> edges)
        {
            List<Vertex> startVertices = new List<Vertex>();
            startVertices = verifyEulerianPath(vertices, edges);
            List<Edge> path = new List<Edge>();

            if (startVertices == null)
            {
                Console.WriteLine("Grafo não euleriano");
            }
            else
            {
                Console.WriteLine("Grafo euleriano");

                if (startVertices.Count() == 0)
                {
                    Console.WriteLine("Inicia pelo primeiro:");
                    Console.WriteLine(edges.First().Name);
                    path = travel(edges.First(), edges);
                }
                else
                {
                    Console.WriteLine("Vertice(s) de inicio:");
                    Console.WriteLine(startVertices.First().Name);
                    Edge startEdge = Edge.GetByVertex(startVertices.First(), edges, new List<Edge>());
                    path = travel(startEdge, edges);
                }
            }
            return path;
        }
    }
}
