﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TesteGrafosPatrick.Models;

namespace TesteGrafosPatrick.Applications
{
    class Dijkstra
    {
        private List<String> searchPath(string startVertexName, List<Edge> startEdges, Vertex endVertex, List<Edge> edges)
        {
            List<Edge> adjacents = new List<Edge>();
            List<String> path = new List<string>();

            foreach (var edge in startEdges)
            {
                adjacents = Edge.GetAllByVertex(edge.EndVertex, edges);
                adjacents = Util.RemoveDoubles(adjacents);

                if (edge.EndVertex == endVertex)
                {
                    path.Add(edge.Name);
                }

                foreach (var adjacent in adjacents)
                {
                    if (adjacent.EndVertex == endVertex)
                    {
                        path.Add(edge.Name + adjacent.Name);

                    }
                }
            }
            return path;
        }

        public List<String> shortestPath(string startVertexName, string endVertexName, List<Vertex> vertices, List<Edge> edges)
        {
            Vertex startVertex = Vertex.GetByName(startVertexName, vertices);
            Vertex endVertex = Vertex.GetByName(endVertexName, vertices);

            List <Edge> startEdges = Edge.GetAllByVertex(startVertex, edges);
            startEdges = Util.RemoveDoubles(startEdges);
            List <String> path = searchPath(startVertexName, startEdges, endVertex, edges);

            return path;
        }
    }
}
