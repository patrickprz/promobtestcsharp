﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TesteGrafosPatrick.Models
{
    class Edge
    {
        private Vertex startVertex;
        private Vertex endVertex;

        public Edge(Vertex startVertex, Vertex endVertex)
        {
            this.startVertex = startVertex;
            this.endVertex = endVertex;
        }

        public String Name
        {
            get { return (this.startVertex.Name + this.endVertex.Name); }
        }

        public Vertex StartVertex
        {
            get { return this.startVertex; }
        }

        public Vertex EndVertex
        {
            get { return this.endVertex; }
        }

        public Double Distance
        {
            get { return Math.Sqrt(Math.Pow(this.endVertex.X - this.startVertex.X, 2) + Math.Pow(this.endVertex.Y - this.startVertex.Y, 2)); }
        }

        public static Edge GetByVertex(Vertex vertex, List<Edge> edges, List<Edge> traveledEdges)
        {
            foreach (var edge in edges)
            {
                if (edge.startVertex.Name == vertex.Name)
                {
                    if (!traveledEdges.Contains(edge))
                    {
                        return edge;
                    }

                }
            }
            return null;
        }

        public static List<Edge> GetAllByVertex(Vertex vertex, List<Edge> edges)
        {
            List<Edge> traveledEdges = new List<Edge>();

            foreach (var edge in edges)
            {
                if (!traveledEdges.Contains(edge))
                {
                    if (edge.StartVertex.Name == vertex.Name)
                    {
                        traveledEdges.Add(edge);
                    }
                    else if (edge.EndVertex.Name == vertex.Name)
                    {
                        Edge aux = new Edge(edge.EndVertex, edge.StartVertex);
                        traveledEdges.Add(edge);
                    }
                }
            }
            return traveledEdges;
        }
    }
}
