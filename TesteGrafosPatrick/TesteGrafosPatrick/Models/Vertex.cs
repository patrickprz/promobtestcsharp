﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TesteGrafosPatrick.Models
{
    class Vertex
    {
        private String name;
        private List<string> vertices;
        private List<string> coordenates;
        

        public Vertex(string name, string coordenates, List<string> vertices)
        {
            this.name = name;
            this.vertices = vertices;
            this.coordenates = coordenates.Split(',').ToList<string>();
        }

        public String Name
        {
            get { return this.name; }
        }
        
        public float X
        {
            get { return float.Parse(this.coordenates[0], CultureInfo.InvariantCulture); }
        }
        
        public float Y
        {
            get { return float.Parse(this.coordenates[1], CultureInfo.InvariantCulture); }
        }

        public List<string> Vertices
        {
            get { return this.vertices; }
        }

   
        public static Vertex GetByName(string name, List<Vertex> vertices)
        {
            foreach (var vertex in vertices)
            {
                if (vertex.Name == name)
                {
                    return vertex;
                }
            }
            return null;
        }

        public static Dictionary<Vertex, int> GetDegree(List<Vertex> vertices, List<Edge> edges)
        {
            Dictionary<Vertex, int> verticesDegrees = new Dictionary<Vertex, int>();
            int degreeCounter = 0;

            foreach (var vertex in vertices)
            {
                foreach (var edge in edges)
                {
                    if (vertex.Name == edge.EndVertex.Name || vertex.Name == edge.StartVertex.Name)
                    {
                        degreeCounter++;
                    }
                }
                verticesDegrees.Add(vertex, degreeCounter);
                degreeCounter = 0;
            }
            return verticesDegrees;
        }
    }
}
