﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TesteGrafosPatrick.Models
{
    class Triangle
    {
        private List<Edge> edges;
        private string name;

        public Triangle(List<Edge> edges)
        {
            this.edges = edges;
            this.name = edges[0].Name + edges[1].Name + edges[2].Name;
        }

        public String Name
        {
            get { return (this.name); }
        }

        public List<Edge> Edges
        {
            get { return (this.edges); }
        }

        public static List<Triangle> GetTriangles(List<Vertex> vertices, List<Edge> edges)
        {
            List<Edge> checkedEdges = new List<Edge>();
            List<Triangle> triangles = new List<Triangle>();
            List<Edge> edgeStack = new List<Edge>();

            foreach (var edge in edges)
            {
                edgeStack.Add(edge);
                while (edgeStack.Count() < 3)
                {
                    if (edgeStack.Count > 0 && edgeStack.Last() != null)
                    {
                        var aux = Edge.GetByVertex(edgeStack.Last().EndVertex, edges, checkedEdges);
                        edgeStack.Add(aux);
                    }
                    else
                    {
                        break;
                    }

                    if (edgeStack.Count() == 3)
                    {
                        if (edgeStack.Last() == null)
                        {
                            checkedEdges.Add(edgeStack.Last());
                            var aux = edgeStack.Last();
                            edgeStack.Remove(aux);
                        }

                        if (edgeStack.Last().EndVertex.Name == edgeStack.First().StartVertex.Name 
                            && edgeStack.Count == 3)
                        {
                            Triangle triangle = new Triangle(new List<Edge> (edgeStack));
                            triangles.Add(triangle);
                        }
                        else
                        {
                            checkedEdges.Add(edgeStack.Last());
                            var aux = edgeStack.Last();
                            edgeStack.Remove(aux);
                        }
                    }
                }
                checkedEdges.Clear();
                edgeStack.Clear();
            }
            return triangles;
        }

        public static Double Area(Triangle triangle)
        {
            List<Edge> edges = triangle.edges;

            Double a = edges[0].Distance;
            Double b = edges[1].Distance;
            Double c = edges[2].Distance;

            Double p = (a + b + c)/2;
            Double area = Math.Sqrt(p * (p - a) * (p - b) * (p - c));

            return area;
        }

        public static Triangle GreaterArea(List<Triangle> triangles)
        {
            if (triangles.Count() != 0)
            {
                Triangle greaterAreaTriangle = triangles.First();
                foreach (var triangle in triangles)
                {
                    if (Triangle.Area(triangle) > Triangle.Area(greaterAreaTriangle))
                    {
                        greaterAreaTriangle = triangle;
                    }
                }
                return greaterAreaTriangle;
            }
            return null;
        }

        public static Dictionary<string, Double> GetAngles(Triangle triangle)
        {
            Dictionary<string, Double> angles = new Dictionary<string, Double>();
            List<Vertex> vertices = new List<Vertex>();
            List<Double> adjacents = new List <Double>();
            Double oppositeValue = 0;
            Double dividend = 0;
            Double divisor = 0;
            Double pr = 0;

            if (triangle != null)
            {

                foreach (var edge in triangle.Edges)
                {
                    vertices.Add(edge.StartVertex);
                }

                foreach (var vertex in vertices)
                {
                    foreach (var edge in triangle.Edges)
                    {
                        if (vertex.Name == edge.StartVertex.Name || vertex.Name == edge.EndVertex.Name)
                        {
                            adjacents.Add(edge.Distance);
                        }
                        else
                        {
                            oppositeValue = edge.Distance;
                        }
                    }
                    dividend = (Math.Pow(adjacents[0], 2)) + (Math.Pow(adjacents[1], 2)) - (Math.Pow(oppositeValue, 2));
                    divisor = 2 * adjacents[0] * adjacents[1];
                    pr = dividend / divisor;
                    angles.Add(vertex.Name, Math.Round((Math.Acos(pr) * (180.0 / Math.PI)), 3));
                    adjacents.Clear();
                    oppositeValue = 0;
                }
            }
            return angles;
        }
    }
}
