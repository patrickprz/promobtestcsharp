﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TesteGrafosPatrick.Applications;
using TesteGrafosPatrick.Models;

namespace TesteGrafosPatrick
{
    class Controller
    {
        public Dictionary<string, string> Application(string jsonPath)
        {
            List<Vertex> vertices = Util.LoadJson(jsonPath);
            DrawAsync(jsonPath);
            Dictionary<string, string> valuesForm = StartApplications(vertices);

            return valuesForm;
        }

        private Dictionary<string, string> StartApplications(List<Vertex> vertices)
        {
            List<Edge> edges = Util.CreateEdges(vertices);
            List<Triangle> triangles = Triangle.GetTriangles(vertices, edges);
            List<Edge> eulerianPath = Eulerian(vertices, edges);
            Dictionary<string, string> valuesForm = new Dictionary<string, string>();
            string eulerPathBuffer = "";
            string trianglesBuffer = "";
            string anglesBuffer = "";
            List<string> triangleSort = new List<string>();

            foreach (var path in eulerianPath)
            {
                if (path != null)
                {
                    eulerPathBuffer = eulerPathBuffer + " " + path.Name;
                }
            }

            valuesForm.Add("eulerPath", eulerPathBuffer);

            foreach (var triangle in triangles)
            {
                if (!triangleSort.Contains(String.Concat(triangle.Name.OrderBy(x => x))))
                {
                    triangleSort.Add(String.Concat(triangle.Name.OrderBy(x => x)));
                    trianglesBuffer = trianglesBuffer + " " + triangle.Name;
                }
            }

            valuesForm.Add("triangles", trianglesBuffer);

            Triangle areaTriangle = Triangle.GreaterArea(triangles);
            if (areaTriangle != null)
            {
                valuesForm.Add("area", areaTriangle.Name + "--- área = " + Math.Round(Triangle.Area(areaTriangle),3));
            }
            else
            {
                valuesForm.Add("area", "Sem triângulo, sem ângulo :(");
            }

            Dictionary<string, Double> angles = Triangle.GetAngles(areaTriangle);

            foreach (var angle in angles)
            {
                anglesBuffer = anglesBuffer + " " + (angle.Key + " = " + angle.Value + "°");
            }

            valuesForm.Add("angles", anglesBuffer);

            return valuesForm;
        }
        
        private async Task DrawAsync(string jsonPath)
        {
            await Util.HttpPostAsync(jsonPath);
        }

        private List<Edge> Eulerian(List<Vertex> vertices, List<Edge> edges)
        {
            Eulerian euler = new Eulerian();
            List<Edge> eulerianPath = euler.EulerianPath(vertices, edges);
            return eulerianPath;
        }
    }
}
