﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TesteGrafosPatrick.Applications;
using TesteGrafosPatrick.Models;

namespace TesteGrafosPatrick
{
    public partial class Form : System.Windows.Forms.Form
    {
        Controller controller = new Controller();

        public Form()
        {
            InitializeComponent();
            this.Size = new Size(1200, 650);
            this.Location = new Point(0, 0);
            
        }

        private void loadFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Json Files|*.json";
            openFileDialog.Title = "Selecione o grafo";

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Dictionary<string, string> valuesForm = controller.Application(openFileDialog.FileName);
                this.eulerTextBox.Text = valuesForm["eulerPath"];
                this.trianglesTextBox.Text = valuesForm["triangles"];
                this.areaTextBox.Text = valuesForm["area"] + "\r\n \r\n";
                this.areaTextBox.AppendText(valuesForm["angles"]);
            }
        }

      
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            webBrowser1.Url = Util.GetUrl();
        }

        private void webBrowser1_DocumentCompleted_1(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            webBrowser1.Refresh();
        }

        private void startVertex_TextChanged(object sender, EventArgs e)
        {

        }

        private void endVertex_TextChanged(object sender, EventArgs e)
        {

        }

        private void minimumButton_Click(object sender, EventArgs e)
        {

        }
    }
}
