﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TesteGrafosPatrick.Models;
using System.Net.Http;

namespace TesteGrafosPatrick
{
    class Util
    {
        public static List<Vertex> LoadJson(string path)
        {
            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                List<Vertex> vertexList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Vertex>>(json);
                return vertexList;
            }
        }

        public static List<Edge> CreateEdges(List<Vertex> vertices)
        {
            List<Edge> edges = new List<Edge>();

            foreach (var startVertex in vertices)
            {
                foreach (var endVertexName in startVertex.Vertices)
                {
                    Vertex endVertex = Vertex.GetByName(endVertexName, vertices);
                    Edge edge = new Edge(startVertex, endVertex);
                    edges.Add(edge);
                }
            }
            return edges;
        }

        public static async Task HttpPostAsync(string path)
        {
            HttpClient client = new HttpClient();
            string json;

            using (StreamReader r = new StreamReader(path))
            {
                json = r.ReadToEnd();
            }

            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            
            var response = await client.PostAsync("http://104.131.25.255:5000/draw/", httpContent);

            var responseString = await response.Content.ReadAsStringAsync();
        }

        public static Uri GetUrl()
        {
            Uri url = new Uri("http://104.131.25.255:5000/image/");
            return url;
        }

        public static List<Edge> RemoveDoubles(List<Edge> edges)
        {
            List<string> nameEdges = new List<string>();
            List<Edge> finalEdges = new List<Edge>();

            foreach (var edge in edges)
            {
                if (!nameEdges.Contains(edge.Name))
                {
                    nameEdges.Add(edge.Name);
                    finalEdges.Add(edge);
                }
            }
            return finalEdges;
        }
    }
}
