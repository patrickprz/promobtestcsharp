﻿namespace TesteGrafosPatrick
{
    partial class Form
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.loadButton = new System.Windows.Forms.Button();
            this.eulerTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.trianglesTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.areaTextBox = new System.Windows.Forms.TextBox();
            this.startVertexTextBox = new System.Windows.Forms.TextBox();
            this.minimumButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.endVertexTextBox = new System.Windows.Forms.TextBox();
            this.minimumTextBox = new System.Windows.Forms.TextBox();
            this.drawButton = new System.Windows.Forms.Button();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // loadButton
            // 
            this.loadButton.BackColor = System.Drawing.SystemColors.MenuBar;
            this.loadButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.loadButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadButton.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.loadButton.Location = new System.Drawing.Point(21, 12);
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(180, 40);
            this.loadButton.TabIndex = 0;
            this.loadButton.Text = "Carregar Grafo";
            this.loadButton.UseVisualStyleBackColor = false;
            this.loadButton.Click += new System.EventHandler(this.loadFile_Click);
            // 
            // eulerTextBox
            // 
            this.eulerTextBox.Location = new System.Drawing.Point(21, 87);
            this.eulerTextBox.Multiline = true;
            this.eulerTextBox.Name = "eulerTextBox";
            this.eulerTextBox.ReadOnly = true;
            this.eulerTextBox.Size = new System.Drawing.Size(180, 100);
            this.eulerTextBox.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(46, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "Caminho Euleriano";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(67, 203);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "Triângulos";
            // 
            // trianglesTextBox
            // 
            this.trianglesTextBox.Location = new System.Drawing.Point(21, 222);
            this.trianglesTextBox.Multiline = true;
            this.trianglesTextBox.Name = "trianglesTextBox";
            this.trianglesTextBox.ReadOnly = true;
            this.trianglesTextBox.Size = new System.Drawing.Size(180, 100);
            this.trianglesTextBox.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(23, 341);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(175, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "Triângulo de maior área";
            // 
            // areaTextBox
            // 
            this.areaTextBox.Location = new System.Drawing.Point(21, 360);
            this.areaTextBox.Multiline = true;
            this.areaTextBox.Name = "areaTextBox";
            this.areaTextBox.ReadOnly = true;
            this.areaTextBox.Size = new System.Drawing.Size(180, 73);
            this.areaTextBox.TabIndex = 10;
            // 
            // startVertexTextBox
            // 
            this.startVertexTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.startVertexTextBox.Location = new System.Drawing.Point(73, 450);
            this.startVertexTextBox.MaxLength = 1;
            this.startVertexTextBox.Name = "startVertexTextBox";
            this.startVertexTextBox.Size = new System.Drawing.Size(22, 20);
            this.startVertexTextBox.TabIndex = 11;
            this.startVertexTextBox.TextChanged += new System.EventHandler(this.startVertex_TextChanged);
            // 
            // minimumButton
            // 
            this.minimumButton.BackColor = System.Drawing.SystemColors.MenuBar;
            this.minimumButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.minimumButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minimumButton.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.minimumButton.Location = new System.Drawing.Point(21, 483);
            this.minimumButton.Name = "minimumButton";
            this.minimumButton.Size = new System.Drawing.Size(180, 40);
            this.minimumButton.TabIndex = 13;
            this.minimumButton.Text = "Menor caminho";
            this.minimumButton.UseVisualStyleBackColor = false;
            this.minimumButton.Click += new System.EventHandler(this.minimumButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(18, 451);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label4.Size = new System.Drawing.Size(49, 16);
            this.label4.TabIndex = 14;
            this.label4.Text = "Início:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(136, 451);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label5.Size = new System.Drawing.Size(37, 16);
            this.label5.TabIndex = 16;
            this.label5.Text = "Fim:";
            // 
            // endVertexTextBox
            // 
            this.endVertexTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.endVertexTextBox.Location = new System.Drawing.Point(179, 450);
            this.endVertexTextBox.MaxLength = 1;
            this.endVertexTextBox.Name = "endVertexTextBox";
            this.endVertexTextBox.Size = new System.Drawing.Size(22, 20);
            this.endVertexTextBox.TabIndex = 15;
            this.endVertexTextBox.TextChanged += new System.EventHandler(this.endVertex_TextChanged);
            // 
            // minimumTextBox
            // 
            this.minimumTextBox.Location = new System.Drawing.Point(21, 529);
            this.minimumTextBox.Multiline = true;
            this.minimumTextBox.Name = "minimumTextBox";
            this.minimumTextBox.ReadOnly = true;
            this.minimumTextBox.Size = new System.Drawing.Size(180, 73);
            this.minimumTextBox.TabIndex = 17;
            // 
            // drawButton
            // 
            this.drawButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drawButton.Location = new System.Drawing.Point(594, 526);
            this.drawButton.Name = "drawButton";
            this.drawButton.Size = new System.Drawing.Size(191, 73);
            this.drawButton.TabIndex = 18;
            this.drawButton.Text = "Desenhar";
            this.drawButton.UseVisualStyleBackColor = true;
            this.drawButton.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(222, 12);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.ScrollBarsEnabled = false;
            this.webBrowser1.Size = new System.Drawing.Size(950, 500);
            this.webBrowser1.TabIndex = 19;
            this.webBrowser1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted_1);
            // 
            // Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 611);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.drawButton);
            this.Controls.Add(this.minimumTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.endVertexTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.minimumButton);
            this.Controls.Add(this.startVertexTextBox);
            this.Controls.Add(this.areaTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.trianglesTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.eulerTextBox);
            this.Controls.Add(this.loadButton);
            this.Name = "Form";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button loadButton;
        private System.Windows.Forms.TextBox eulerTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox trianglesTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox areaTextBox;
        private System.Windows.Forms.TextBox startVertexTextBox;
        private System.Windows.Forms.Button minimumButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox endVertexTextBox;
        private System.Windows.Forms.TextBox minimumTextBox;
        private System.Windows.Forms.Button drawButton;
        private System.Windows.Forms.WebBrowser webBrowser1;
    }
}

